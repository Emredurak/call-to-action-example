# call-to-action



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description | Type     | Default     |
| ----------------- | ------------------- | ----------- | -------- | ----------- |
| `buttonText`      | `button-text`       |       Text for call to action button      | `string` | `undefined` |
| `contactBoxTitle` | `contact-box-title` |     Text for contact box        | `string` | `undefined` |
| `ctaDescription`  | `cta-description`   |      Description for call to action content.       | `string` | `undefined` |
| `ctaTitle`        | `cta-title`         |       Title for call to action content.      | `string` | `undefined` |
| `headline`        | `headline`          |   Headline of the component          | `string` | `undefined` |


----------------------------------------------

<style>.sbdocs-h1{display: none;}</style>
