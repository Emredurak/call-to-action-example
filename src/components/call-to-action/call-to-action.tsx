import {Component, Host, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'call-to-action',
  styleUrl: 'call-to-action.scss',
  shadow: true,
  assetsDirs: ['assets'],
})

export class CallToAction {
  @Prop() buttonText: string = 'Button Text';
  @Prop() contactBoxTitle: string = 'Contact Box Title';
  @Prop() contactBoxDescription: string = 'Contact Box Description';
  @Prop() ctaTitle: string = 'CtA Title';
  @Prop() ctaDescription: string = 'CtA Description';
  @Prop() headline: string = 'Headline';
  @State() open: boolean = Boolean(false);

  private openContact() {
    if(!this.open){
      this.open = true;
      setTimeout(() => window.scrollBy({
        top: 200,
        left: 0,
        behavior : "smooth"
      }),350);

    }
  }

  private closeContact() {
    if(this.open){
      this.open = false;
    }
  }

  render() {
    return (
      <Host>
        <div id={'wrapper'}>
            <h1 id={'headline'}>{this.headline}</h1>
            <div class={'grid-container'}>
              <div class={'info-section'}>
                <h2 id={'ctaTitle'}>{this.ctaTitle}</h2>
                <p id={'ctaDescription'}>{this.ctaDescription}</p>
                <button id={'ctaButton'} onClick={() => this.openContact()}>{this.buttonText}</button>
              </div>
              <img class={'image-section'}
                   src={'https://visitrainier.com/wp-content/uploads/2020/03/005-ZAC_6160-Pano.jpg'}/>
            </div>
            <div id={'contactBox'} class={{'anim-in': this.open, 'anim-out': !this.open}}>
              <div class={'contact-box-header'}>
                <div class={'cursor'}></div>
                <div id={'closeIcon'} onClick={() => this.closeContact()}>
                  <div class='close-icon-l1'>
                    <div class="close-icon-l2"></div>
                  </div>
                </div>
              </div>
              <div class={'contact-box-content-wrapper'}>
                <h2 class={'contact-box-title'}>{this.contactBoxTitle}</h2>
                <p class={'contact-box-description'}> Give some contact information: {this.contactBoxDescription}</p>
              </div>
            </div>
          </div>
      </Host>
    );
  }

}
